This generates a g++ executable with bounds-checking enabled, requires g++ 4.8.0 or later.

to build enter
make [ -j <n> ]
at the command prompt

the three tests included here should crash due to array bounds exceeded
crashBoundsStack.in
crashBoundsStatic.in
crashBoundsVector.in
